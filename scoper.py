#!/usr/bin/env python
from __future__ import print_function, with_statement
import sys
import operator
import socket
import re
import netaddr

__author__ = 'Ethan Robish'

# TODO:
# test Python 3 compatibility
# keep track of hostname and print it with the corresponding IPs
# add options for command line switches
#   perform reverse dns lookups on IPs
#   list out individual IP addresses instead of cidr networks
#   output to file(s)

USAGE = """\
Scoper takes multiple files, each containing a set of IPs, IP ranges, or hostnames.
Scoper then outputs the IPs that exist only in each set, followed by the IPs that
exist in all sets.

Usage: %s file1 file2 [file3 ...]

Each file should contain one IP, range, or hostname per line.  IP ranges are
pretty flexible.  If nmap can understand it, chances are Scoper can too.

Note: netaddr library is required (pip install netaddr || easy_install netaddr)
""" % (sys.argv[0])

def resolve_hostname(d):
    """
    This method returns an array containing
    one or more IP address strings that respond
    as the given domain name

    source: http://stackoverflow.com/questions/3837744/how-to-resolve-dns-in-python
    """
    try:
        data = socket.gethostbyname_ex(d)
        return data[2]
    except Exception:
        # fail gracefully!
        return []

def parse_file(inf):
    """Returns a netaddr.IPSet"""
    ip_cidr_regex = re.compile(r"""^
        (
            (
                [0-9]
                |[1-9][0-9]
                |1[0-9]{2}
                |2[0-4][0-9]
                |25[0-5]
            )
            \.
        ){3}
        (
            [0-9]
            |[1-9][0-9]
            |1[0-9]{2}
            |2[0-4][0-9]
            |25[0-5]
        )
        (/(3[012]|[12]\d|\d))?
    $""", re.VERBOSE)

    hostname_regex = re.compile(r"""^
        (
            (
                [a-zA-Z0-9]
                |[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]
            )
            \.
        )*
        (
            [A-Za-z0-9]
            |[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]
        )
    $""", re.VERBOSE)

    ip_set = netaddr.IPSet()
    for line in inf:
        # get rid of protocol
        if '//' in line:
            line = line.split('/')[-1]
        # get rid of trailing newline and any other whitespace; also get rid of ports
        line = line.strip().strip('/').split(':')[0]
        # check if string is something like 192.168.1.1-255
        if netaddr.valid_nmap_range(line):
            ip_set |= netaddr.IPSet(netaddr.iter_nmap_range(line))
        # check if string is a glob (eg. 192.168.1.*)
        elif netaddr.valid_glob(line):
            ip_set |= netaddr.IPSet(netaddr.glob_to_cidrs(line))
        # check if it is a single IP address or CIDR network
        elif ip_cidr_regex.match(line):
            ip_set.add(line)
        # check if it is a hostname and resolve the hostname
        elif hostname_regex.match(line):
            ip_set |= netaddr.IPSet(resolve_hostname(line))

    return ip_set

def main(*files):
    ip_sets = []
    for filename in files:
        with open(filename) as inf:
            # each file gets its own set
            ip_sets.append(parse_file(inf))

    # find all the IPs the sets have in common
    # reduce(operator.__and__, ip_sets) is like sum(ip_sets) but with & (set union) instead of +
    ip_set_intersection = sorted(reduce(operator.__and__, ip_sets))

    # find the IPs that are unique to each set (ie. do not exist in any other sets)
    ip_set_uniques = []
    for i, ip_set in enumerate(ip_sets):
        # reduce(operator.__sub__, [ip_set] + ip_sets[:i] + ip_sets[i+1:])
        # is like sum(ip_sets) but with - (set difference) instead of +
        # [ip_set] + ip_sets[:i] + ip_sets[i+1:] ensures that we start out with the current set
        # and then subtract off all other sets (excluding itself)
        ip_set_uniques.append(sorted(reduce(operator.__sub__, [ip_set] + ip_sets[:i] + ip_sets[i+1:])))

    for i, ip_set_diff in enumerate(ip_set_uniques):
        print('Unique to set #%d' % (i+1))
        # print out individual IP addresses
        #print('\n'.join(map(str, ip_set_diff))) if ip_set_diff else print('None')
        # print out CIDR networks
        print('\n'.join(map(str, netaddr.cidr_merge(ip_set_diff))))
        print()

    print('Common to all sets')
    # print out individual IP addresses
    #print('\n'.join(map(str, ip_set_intersection))) if ip_set_intersection else print('None')
    # print out CIDR networks
    print('\n'.join(map(str, netaddr.cidr_merge(ip_set_intersection))))

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(USAGE)
        sys.exit('Error: too few arguments')

    main(*sys.argv[1:])
