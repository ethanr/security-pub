#!/usr/bin/env python
from __future__ import print_function, with_statement
import sys
import operator
import socket
import re
import netaddr

__author__ = 'Ethan Robish'

# TODO:
# test Python 3 compatibility
# keep track of hostname and print it with the corresponding IPs
# add options for command line switches
#   perform reverse dns lookups on IPs
#   list out individual IP addresses instead of cidr networks
#   output to file(s)
#
# Check that domain lookups resolve to all possible IP addresses
#
# Convert to command structure to allow for chaining of arguments
#   commands: size, equal
# Use prefix notation

USAGE = """\
Scoper takes multiple files, each containing a set of IPs, IP ranges, or hostnames.
Scoper then outputs the IPs that exist only in each set, followed by the IPs that
exist in all sets.

Usage: %s file1 file2 [file3 ...]

Each file should contain one IP, range, or hostname per line.  IP ranges are
pretty flexible.  If nmap can understand it, chances are Scoper can too.

Note: netaddr library is required (pip install netaddr || easy_install netaddr)
""" % (sys.argv[0])

def union():
    main(lambda ip_sets: sorted(reduce(operator.__or__, ip_sets)))

def intersection():
    main(lambda ip_sets: sorted(reduce(operator.__and__, ip_sets)))

def difference():
    main(lambda ip_sets: sorted(reduce(operator.__sub__, ip_sets)))

def size():
    main(lambda ip_sets: [len(ip_set) for ip_set in ip_sets])

def equal():
    main(lambda ip_sets: [all(map(operator.__eq__, ip_sets, ip_sets[1:] + ip_sets[:1]))])

def cidr_reduce():
    with open(sys.argv[1]) as inf:
        print('\n'.join(map(str, netaddr.cidr_merge(parse_file(inf)))))

def resolve_hostname(d):
    """
    This method returns an array containing
    one or more IP address strings that respond
    as the given domain name

    source: http://stackoverflow.com/questions/3837744/how-to-resolve-dns-in-python
    """
    try:
        data = socket.gethostbyname_ex(d)
        return data[2]
    except Exception:
        # fail gracefully!
        return []

def parse_file(inf):
    """Returns a netaddr.IPSet"""
    ip_cidr_regex = re.compile(r"""^
        (
            (
                [0-9]
                |[1-9][0-9]
                |1[0-9]{2}
                |2[0-4][0-9]
                |25[0-5]
            )
            \.
        ){3}
        (
            [0-9]
            |[1-9][0-9]
            |1[0-9]{2}
            |2[0-4][0-9]
            |25[0-5]
        )
        (/(3[012]|[12]\d|\d))?
    $""", re.VERBOSE)

    hostname_regex = re.compile(r"""^
        (
            (
                [a-zA-Z0-9]
                |[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9]
            )
            \.
        )*
        (
            [A-Za-z0-9]
            |[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]
        )
    $""", re.VERBOSE)

    ip_set = netaddr.IPSet()
    for line in inf:
        # get rid of protocol
        if '//' in line:
            line = line.split('/')[-1]
        # get rid of trailing newline and any other whitespace; also get rid of ports
        line = line.strip().strip('/').split(':')[0]
        # check if string is something like 192.168.1.1-255
        if netaddr.valid_nmap_range(line):
            ip_set |= netaddr.IPSet(netaddr.iter_nmap_range(line))
        # check if string is a glob (eg. 192.168.1.*)
        elif netaddr.valid_glob(line):
            ip_set |= netaddr.IPSet(netaddr.glob_to_cidrs(line))
        # check if it is a single IP address or CIDR network
        elif ip_cidr_regex.match(line):
            ip_set.add(line)
        # check if it is a hostname and resolve the hostname
        elif hostname_regex.match(line):
            ip_set |= netaddr.IPSet(resolve_hostname(line))

    return ip_set

def main(op):
    files = sys.argv[1:]
    ip_sets = []
    for filename in files:
        with open(filename) as inf:
            # each file gets its own set
            ip_sets.append(parse_file(inf))

    result = op(ip_sets)
    print('\n'.join(map(str, result)))

